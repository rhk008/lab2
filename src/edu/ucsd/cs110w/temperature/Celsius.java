/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author rhk008
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
	      super(t);
	}
	public String toString()
	{
	      // TODO: Complete this method
		  return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this; //new Celsius(((this.getValue()-32)*5)/9);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(((this.getValue()*9)/5) + 32);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}
