/**
 * 
 */
package edu.ucsd.cs110w.temperature;

import java.beans.FeatureDescriptor;

/**
 * TODO (rhk008): write class javadoc
 * 
 * @author rhk008
 * 
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) {
		super(t);
	}

	public String toString() {
		// TODO: Complete this method
		return "" + this.getValue() + " K";
	}

	@Override
	public Temperature toCelsius() {
		// TODO: Complete this method
		return new Celsius(this.getValue() - (float)273.15);
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO: Complete this method
		return new Fahrenheit((this.getValue() * (float)1.8) - (float)459.67);
	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return this;
	}
}
