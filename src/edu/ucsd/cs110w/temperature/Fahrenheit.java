/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author rhk008
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		  super(t);
	}
	public String toString()
	{
	      // TODO: Complete this method
	      return "" + this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(((this.getValue()-32)*5)/9);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}
